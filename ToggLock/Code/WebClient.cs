﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace ToggLock.Code {

    class WebClient {
        private static CookieContainer _cookies = new CookieContainer();

        public void post(Type cast, String page, String data, WebCallback callback) {

            var request = (HttpWebRequest)WebRequest.Create("https://www.toggl.com/api/v6/" + page);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.CookieContainer = _cookies;
            request.ContentLength = data.Length;

            using (var writer = new StreamWriter(request.GetRequestStream(), Encoding.ASCII)) {
                writer.Write(data);
            }

			execute(request,cast,callback);
        }

        public void get(Type cast, String page, WebCallback callback) {
            var request = (HttpWebRequest)WebRequest.Create("https://www.toggl.com/api/v6/" + page);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.CookieContainer = _cookies;
			execute(request,cast,callback);
        }

        private void execute(HttpWebRequest request,Type cast, WebCallback callback) {
			try{
				new System.Threading.Thread(() => executeThread(request,cast,callback)).Start();
			} catch (Exception) {
				callback.error();
			}
        }

        private void executeThread(HttpWebRequest request,Type cast,WebCallback callback) {
            var response = String.Empty;
            HttpWebResponse authResponse = null;
			authResponse = (HttpWebResponse)request.GetResponse();

            var stream = authResponse.GetResponseStream();
            if (stream == null)
                throw new Exception();

            var reader = new StreamReader(stream, Encoding.UTF8);
			response = reader.ReadToEnd();

            var jss = new JavaScriptSerializer();
            var ret = jss.Deserialize<dynamic>(response);
            String js2 = jss.Serialize(ret["data"]);
            dynamic obj = jss.Deserialize(js2, cast);

			if(obj != null)
				callback.success(obj);
			else 
			    callback.error();
        }
    }
}
