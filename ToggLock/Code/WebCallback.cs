﻿namespace ToggLock.Code {
    public interface WebCallback {
        void success(dynamic data);
        void error();
    }
}
