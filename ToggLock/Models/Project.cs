﻿namespace ToggLock.Models {
    public class Project {
        public int id { get; set; }
        public object estimated_workhours { get; set; }
        public bool automatically_calculate_estimated_workhours { get; set; }
        public string name { get; set; }
        public bool billable { get; set; }
        public Workspace workspace { get; set; }
        public string client_project_name { get; set; }
        public bool is_active { get; set; }
        public bool is_private { get; set; }
        public string updated_at { get; set; }
        public double fixed_fee { get; set; }
        public double hourly_rate { get; set; }
        public bool is_fixed_fee { get; set; }
    }
}
