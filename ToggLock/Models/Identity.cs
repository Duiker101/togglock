﻿namespace ToggLock.Models {
    class Identity {
        public string email { get; set; }
        public string date_format { get; set; }
        public bool new_time_entries_start_automatically { get; set; }
        public string language { get; set; }
        public string api_token { get; set; }
        public string fullname { get; set; }
        public int time_entry_retention_days { get; set; }
        public string jquery_date_format { get; set; }
        public int default_workspace_id { get; set; }
        public string jquery_timeofday_format { get; set; }
        public int beginning_of_week { get; set; }
        public int id { get; set; }
        public string timeofday_format { get; set; }
    }
}
