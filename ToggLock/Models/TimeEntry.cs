﻿using System.Collections.Generic;

namespace ToggLock.Models
{
    public class TimeEntry {
        public long duration { get; set; }
        public bool billable { get; set; }
        public Workspace workspace { get; set; }
        public string stop { get; set; }
        public int id { get; set; }
        public Project project { get; set; }
        public string start { get; set; }
        public List<string> tag_names { get; set; }
        public string description { get; set; }
        public string ignore_start_and_stop { get; set; }
        public string created_with { get; set; }
    }
}
