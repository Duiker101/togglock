﻿namespace ToggLock.Controls
{
    partial class DayControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.entry1 = new ToggLock.Controls.EntryControl();
            this.SuspendLayout();
            // 
            // entry1
            // 
            this.entry1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.entry1.Location = new System.Drawing.Point(0, 0);
            this.entry1.Name = "entry1";
            this.entry1.Size = new System.Drawing.Size(20, 89);
            this.entry1.TabIndex = 0;
            // 
            // Day
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.entry1);
            this.Name = "Day";
            this.Size = new System.Drawing.Size(174, 504);
            this.ResumeLayout(false);

        }

        #endregion

        private EntryControl entry1;

    }
}
